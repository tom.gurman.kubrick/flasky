from flask import Flask, jsonify, request
import os

app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))   # Sets relative path to location of this file

@app.route('/')
def home():
    return jsonify(data='My MLE02 Home Page - Welcome!')



# Don't run this script if this module has been imported
# Only run the app if the script is being executed directly
if __name__ == '__main__':
    app.run()
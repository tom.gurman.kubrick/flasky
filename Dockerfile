FROM python:3.8-alpine

RUN mkdir /code

COPY requirements.txt /code/

RUN pip install -r/copy/requirements.txt

COPY flaskyapp.py /code/

CMD ["python","/code/flaskyapp.py"]
